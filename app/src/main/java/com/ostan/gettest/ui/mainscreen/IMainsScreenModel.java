package com.ostan.gettest.ui.mainscreen;

import android.location.Location;

import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMainsScreenModel {

    void onStart();
    void onStop();
    void onCurrentLocationRequested(boolean isCalledFromOnMapReady);
    void onNearbyPlacesRequested(Location location);
    void onAddressRequested(LocationType type, Location location);

    void onRouteRequested(Location origin, Location destination);

    void onPLaceDetailsRequested(String placeId);
}


package com.ostan.gettest.models.places;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class OpeningHours {

    @SerializedName("open_now")
    @Expose
    protected Boolean openNow;

    public Boolean getOpenNow() {
        return openNow;
    }

}

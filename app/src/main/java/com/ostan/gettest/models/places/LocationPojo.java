
package com.ostan.gettest.models.places;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class LocationPojo {

    @SerializedName("lat")
    @Expose
    protected Double lat;
    @SerializedName("lng")
    @Expose
    protected Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }


    public LatLng toLatLang(){
        return new LatLng(lat,lng);
    }

    public android.location.Location toGoogleLocation(){
        android.location.Location location =  new android.location.Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }

    public static LocationPojo fromGoogleLocation(android.location.Location src) {
        LocationPojo result = new LocationPojo();
        result.lat = src.getLatitude();
        result.lng = src.getLongitude();
        return result;
    }
}

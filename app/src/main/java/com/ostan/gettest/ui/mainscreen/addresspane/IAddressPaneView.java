package com.ostan.gettest.ui.mainscreen.addresspane;

import com.ostan.gettest.models.addresses.AddressData;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public interface IAddressPaneView {
    /**
     * Triiger view to present to user the address that is passed
     * @param addressData - address to be presented if passed @null addressPane will be hidden
     */
    void showAddress(AddressData addressData);

    /**
     * Show route button
     * @param toShow - true when needs to be shown, fals ewhen needs to be hidden
     */
    void showRouteButton(boolean toShow);

    /**
     * Show loading indication
     * @param toShow - true when needs to be shown, fals ewhen needs to be hidden
     */
    void showLoading(boolean toShow);
}

package com.ostan.gettest.ui.mainscreen.map;

import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;

import java.util.List;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMapController {
    void addPlaces(List<Place> markers);
    void centerOnPosition(LatLng location, float zoom);
    void onSearchAddressArrived(Details details, float zoom);
    void drawRoute(Route route);
}

package com.ostan.gettest.ui.mainscreen;


import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.models.places.Place;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMainScreenPresenter {


    void onStart();
    void onStop();
    void onMapCenterChanged(LatLng currentCenterLocation);
    void onMarkerClicked(Place data);
    void onRouteButtonClicked(LocationPojo origin, LocationPojo destination);
    void onMapReady();
    void onLocationButtonClicked();
    void onSearchButtonClicked();

    void onSearchResultReturned(Prediction prediction);
}

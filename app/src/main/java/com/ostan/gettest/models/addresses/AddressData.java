
package com.ostan.gettest.models.addresses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressData {

    @SerializedName("address_components")
    @Expose
    protected List<AddressComponent> addressComponents = null;
    @SerializedName("formatted_address")
    @Expose
    protected String formattedAddress;
    @SerializedName("geometry")
    @Expose
    protected Geometry geometry;
    @SerializedName("place_id")
    @Expose
    protected String placeId;
    @SerializedName("types")
    @Expose
    protected List<String> types = null;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}

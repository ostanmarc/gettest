package com.ostan.gettest.ui.mainscreen.addresspane;

import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

/**
 * Created by marc.ostan on 5/16/2017.
 */

public interface IAddressPane {
    /**
     * Pass the address data to presenter
     * @param type - address type
     * @param data - address data
     */
    void onAddressReceived(LocationType type, AddressData data);

    /**
     * Notify Pane presenter about loading that is started or ended
     * @param isStarted
     */
    void loadingStarted(boolean isStarted);
}

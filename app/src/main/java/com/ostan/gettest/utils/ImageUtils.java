package com.ostan.gettest.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by marco on 17/05/2017.
 */

public class ImageUtils {

    public static Observable<Bitmap> decodeImage(ResponseBody body) {
        try {

            return Observable.just(BitmapFactory.decodeStream(body.byteStream()));

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}

package com.ostan.gettest.ui.search;

import android.util.Log;

import com.ostan.gettest.models.autocomplete.Prediction;

import java.util.List;

/**
 * Created by marco on 18/12/2016.
 */

public class AddressSearchModel extends AbstractBaseSearchModel<Prediction> {

    public AddressSearchModel(ISearchController presenter) {
        super(presenter);
    }

    @Override
    public void onNext(List<Prediction> response) {
        super.onNext(response);

        for (Prediction iterator: response) {
            Log.i("LOG","PREDICTION ARRIVED:" + iterator.getDescription());
        }
    }
}

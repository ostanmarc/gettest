package com.ostan.gettest.ui.search.recycler;

/**
 * Created by marco on 15/07/2016.
 */
public interface IResultsEventsCallback<T> {
     /***
      * notify callback that result was clicked
      * @param item
      */
     void onResultWasClicked(T item);
}

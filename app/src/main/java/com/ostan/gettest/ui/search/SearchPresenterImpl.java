package com.ostan.gettest.ui.search;

import android.location.Location;
import android.view.View;
import android.widget.EditText;

import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.networking.APIConnector;

import java.util.List;

/**
 * Created by marco on 17/12/2016.
 */

public class SearchPresenterImpl implements ISearchPresenter<Prediction>, ISearchController<Prediction> {


    SearchActivity activity;
    ISearchView view;
    Location currentLocation;
    AbstractBaseSearchModel model;

    public SearchPresenterImpl(SearchActivity activity, View view, Location location) {
        this.activity = activity;
        this.view = new SearchViewImpl(this, view);
        this.model = new AddressSearchModel(this);
        this.currentLocation = location;
    }


    @Override
    public void onQuerryTextEdited(String currentQuerryString) {
        APIConnector connector = new APIConnector();
        connector.getAutocompletePredictions(currentQuerryString, currentLocation, model);
    }

    @Override
    public void onResultClicked(Prediction selectedResult) {
        activity.finishAndNotify(selectedResult);
    }


    @Override
    public void onNewResponseArrived(List<Prediction> items) {
        for(Prediction item: items){
            view.addAddressItem(items);
        }
    }

    @Override
    public void setSearchField(EditText editText) {

        this.view.activateQuerryChangesListening(editText);
    }
}

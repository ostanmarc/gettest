package com.ostan.gettest.ui.search;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ostan.gettest.R;
import com.ostan.gettest.models.autocomplete.Prediction;

import org.parceler.Parcels;


public class SearchActivity extends AppCompatActivity {


    public static final String EXTRA_RUNNING_MODE = "runningMode";
    public static final String EXTRA_CURRENT_LOCATION = "currentLocation";
    public static final String EXTRA_SEARCH_RESULT = "item";


    private ISearchController callback;

    RunningMode runningMode;
    Location searchAtLocation;

    public static enum RunningMode {
        addressSearch,
        startPointSearch;


        public static RunningMode fromInt(int src) {
            if (src > RunningMode.values().length
                    || src <= 0) {
                return addressSearch;
            }
            return RunningMode.values()[src];
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        proccessIncomingIntent(getIntent());

        handleActionBar(this);

        initialize();
    }


    private void handleActionBar(AppCompatActivity activity) {

        LayoutInflater inflater = LayoutInflater.from(activity);
        android.support.v7.app.ActionBar actionBar = activity.getSupportActionBar();
        View view;

        view = inflater.inflate(R.layout.action_bar_search_layout, null);

        actionBar.setCustomView(view, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT));
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");

        EditText et = (EditText) view.findViewById(R.id.address_et);

        switch (runningMode) {
            case addressSearch:
                et.setHint(getString(R.string.default_value_destination_address));
                break;
            case startPointSearch:
                et.setHint(getString(R.string.default_value_origin_address));
                break;
            default: {
            }
            break;
        }

    }


    private void initialize() {
        switch (runningMode) {
            case startPointSearch:
            case addressSearch: {
                callback = new SearchPresenterImpl(this, findViewById(R.id.search_root_view), searchAtLocation);
                callback.setSearchField((EditText) findViewById(R.id.address_et));
            }
            break;
        }
    }

    private void proccessIncomingIntent(Intent intent) {
        runningMode = RunningMode.addressSearch;
        searchAtLocation = intent.getParcelableExtra(EXTRA_CURRENT_LOCATION);
    }

    protected void finishAndNotify(Prediction item) {

        Intent intent = new Intent();
        intent.putExtra(EXTRA_SEARCH_RESULT, Parcels.wrap(item));
        intent.putExtra(EXTRA_RUNNING_MODE, runningMode);
        setResult(RESULT_OK, intent);

        finish();
    }


}

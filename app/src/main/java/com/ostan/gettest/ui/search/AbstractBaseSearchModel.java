package com.ostan.gettest.ui.search;

import com.ostan.gettest.ui.search.ISearchController;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;

/**
 * Created by marco on 18/12/2016.
 */

public abstract class AbstractBaseSearchModel<T> implements Observer<List<T>> {

    protected ISearchController<T> presenter;

    public AbstractBaseSearchModel(ISearchController presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onNext(List<T> t) {
        List<T> data = new ArrayList<>();
        data.addAll(t);
        presenter.onNewResponseArrived(data);
    }

}

package com.ostan.gettest.utils;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ostan.gettest.R;

/**
 * Created by marco on 28/02/2017.
 */

public class BindingAdapters {

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView view, String url) {
        // Load the user's image
        Glide.with(view.getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.drawable.ic_home_black_24dp)
                .into(view);
    }

    @BindingAdapter("bind:bitmap")
    public static void setImage(ImageView view, Bitmap bitmap) {
      view.setImageBitmap(bitmap);
    }
}


package com.ostan.gettest.models.places;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Photo {

    @SerializedName("height")
    @Expose
    protected Integer height;
    @SerializedName("html_attributions")
    @Expose
    protected List<String> htmlAttributions = null;
    @SerializedName("photo_reference")
    @Expose
    protected String photoReference;
    @SerializedName("width")
    @Expose
    protected Integer width;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public List<String> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<String> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

}

package com.ostan.gettest.ui.search.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ostan.gettest.databinding.SearchResultItemBinding;
import com.ostan.gettest.models.autocomplete.Prediction;

/**
 * Created by marco on 18/12/2016.
 */

public class ItemViewHolder  extends RecyclerView.ViewHolder   {

    final SearchResultItemBinding binding;
    IResultsEventsCallback listener;

    public ItemViewHolder(IResultsEventsCallback listener, SearchResultItemBinding binding) {
        super(binding.getRoot());
        this.listener = listener;
        this.binding = binding;

    }

    void bindViewItemImpl(final Prediction item) {
        binding.setPrediction(item);
        binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onResultWasClicked(item);
            }
        });
    }
}

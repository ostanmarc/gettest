
package com.ostan.gettest.models.placedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlacesDetailsResponse {

    @SerializedName("html_attributions")
    @Expose
    protected List<Object> htmlAttributions = null;
    @SerializedName("result")
    @Expose
    protected Details details;
    @SerializedName("status")
    @Expose
    protected String status;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

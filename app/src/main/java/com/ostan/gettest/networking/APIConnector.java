package com.ostan.gettest.networking;

import android.graphics.Bitmap;
import android.location.Location;
import android.util.Log;

import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.addresses.GeoCodingResponse;
import com.ostan.gettest.models.autocomplete.AutoCompleteResponse;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.placedetails.PlacesDetailsResponse;
import com.ostan.gettest.models.placedetails.Photo;
import com.ostan.gettest.models.places.PlacesResponse;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.models.routes.RoutesResponse;
import com.ostan.gettest.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.ostan.gettest.networking.APIConstants.API_KEY;
import static com.ostan.gettest.networking.APIConstants.AUTOCOMPLETE_ENDPOINT;
import static com.ostan.gettest.networking.APIConstants.BASE_URL;
import static com.ostan.gettest.networking.APIConstants.GEOCODING_ENDPOINT;
import static com.ostan.gettest.networking.APIConstants.IMAGE_ENDPOINT;
import static com.ostan.gettest.networking.APIConstants.PLACES_API_ENDPOINT;
import static com.ostan.gettest.networking.APIConstants.PLACE_DETAILS_ENDPOINT;
import static com.ostan.gettest.networking.APIConstants.ROUTES_API_ENDPOINT;

/**
 * Created by marco on 17/12/2016.
 */

public class APIConnector extends AbstractApiConnector {

    public void getNearbyPlaces(Location loc, String keyword, Observer<PlacesResponse> observer) {
        try {
            getReactiveNetworkInterfaceInstance(PLACES_API_ENDPOINT)
                    .getNearbyPlaces(loc.getLatitude() + "," + loc.getLongitude(), 5000, API_KEY, keyword)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void getAddressByLocation(Location loc, final Observer<AddressData> observer) {
        try {

            getNetworkInterfaceInstance(GEOCODING_ENDPOINT)
                    .reverseLocation(loc.getLatitude() + "," + loc.getLongitude(), API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io()).subscribe(new Observer<GeoCodingResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(GeoCodingResponse geoCodingResponse) {
                    List<AddressData> results = geoCodingResponse.getResults();
                    if (!results.isEmpty()) {
                        Observable.just(results.get(0)).subscribeOn(AndroidSchedulers.mainThread()).subscribe(observer);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public List<Prediction> getAutocompletePredictions(String input, Location loc) {
        try {
            return
                    getNetworkInterfaceInstance(AUTOCOMPLETE_ENDPOINT)
                            .autoCompleteRequest(input, loc.getLatitude() + "," + loc.getLongitude(), API_KEY)
                            .execute().body().getPredictions();

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void getPLaceDEtails(String id, final Observer<Details> observer) {
        try {
            getNetworkInterfaceInstance(PLACE_DETAILS_ENDPOINT)
                    .placeDeteailsRequest(id, API_KEY)
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<PlacesDetailsResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(PlacesDetailsResponse placesDetailsResponse) {
                            Observable.just(placesDetailsResponse.getDetails())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(observer);
                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void getAutocompletePredictions(String input, Location loc, final Observer<List<Prediction>> observer) {
        try {
            getReactiveNetworkInterfaceInstance(AUTOCOMPLETE_ENDPOINT)
                    .autoCompleteRequestRx(input, loc.getLatitude() + "," + loc.getLongitude(), API_KEY)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(new Observer<AutoCompleteResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(AutoCompleteResponse autoCompleteResponse) {
                            Observable.just(autoCompleteResponse.getPredictions())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(observer);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getRoutesRx(Location origin, Location destination, Observer<List<Route>> observer) {
        getRoutesRx(origin.getLatitude() + "," + origin.getLongitude(), destination.getLatitude() + "," + destination.getLongitude(), observer);
    }

    public void getRoutesRx(String origin, String dst, final Observer<List<Route>> observer) {
        try {
            getReactiveNetworkInterfaceInstance(ROUTES_API_ENDPOINT)
                    .routesRequestRx(origin, dst, API_KEY).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<RoutesResponse>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(RoutesResponse routesResponse) {
                    Observable.just(routesResponse.getRoutes()).observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getImage(Photo photo, final Observer<Bitmap> bitmapObserver) {

        getNetworkInterfaceInstance(IMAGE_ENDPOINT)
                .getImageDetails(photo.getPhotoReference(), photo.getHeight() + "", API_KEY)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            ImageUtils.decodeImage(response.body())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(bitmapObserver);

                        } catch (Exception e) {
                            Log.d("onResponse", "There is an error");
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });


    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}

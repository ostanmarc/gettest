package com.ostan.gettest.ui.mainscreen;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.SupportMapFragment;
import com.ostan.gettest.R;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.ui.search.SearchActivity;

import org.parceler.Parcels;

import static com.ostan.gettest.ui.search.SearchActivity.EXTRA_CURRENT_LOCATION;
import static com.ostan.gettest.ui.search.SearchActivity.EXTRA_SEARCH_RESULT;

public class MapsActivity extends FragmentActivity implements IMainScreenCallback {


    IMainScreenPresenter mainScreenPresenter;

    public static final int SEARCH_REQUEST_CODE = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SEARCH_REQUEST_CODE && resultCode == RESULT_OK) {
            Prediction resultPrediction = Parcels.unwrap(data.getExtras().getParcelable(EXTRA_SEARCH_RESULT));
            mainScreenPresenter.onSearchResultReturned(resultPrediction);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.main_map_fragment);

        mainScreenPresenter = new MainScreenPresenterImpl(mapFragment, this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mainScreenPresenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainScreenPresenter.onStop();
    }


    @Override
    public void onSearchRequested(Location location) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(EXTRA_CURRENT_LOCATION, location);
        startActivityForResult(intent, SEARCH_REQUEST_CODE);
    }

    @Override
    public FragmentActivity getActivity() {
        return this;
    }
}

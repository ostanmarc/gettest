package com.ostan.gettest.ui.mainscreen.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMapCallback {
    void onMapReady();
    void onMapCenterChanged(LatLng location);
    void onMarkerClicked(Marker position);
    void onCameraDragged(boolean isDragging);
    
}

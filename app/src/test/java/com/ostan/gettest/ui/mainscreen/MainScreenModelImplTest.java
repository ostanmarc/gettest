package com.ostan.gettest.ui.mainscreen;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.utils.AppStateSharedPrefsImpl;
import com.ostan.gettest.utils.IAppstateSharedPrefs;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 17/05/2017.
 */
public class MainScreenModelImplTest {



    IAppstateSharedPrefs sharedPrefs = mock(AppStateSharedPrefsImpl.class);
    GoogleApiClient googleApiClient = mock(GoogleApiClient.class);
    IMainScreenModelCallback callback = mock(MainScreenPresenterImpl.class);


    // Shared prefs dependency with mocked Location value
    IAppstateSharedPrefs testSharedPRefsWithSavedLocation = new IAppstateSharedPrefs() {

        TestLocationPojo pojo = new TestLocationPojo(12d, 12d);

        @Override
        public void setLastKnownLocation(LocationPojo location) {

        }

        @Override
        public LocationPojo getLastKnownLocation() {
            return pojo;
        }
    };


    @Test
    public void OnCurrentLocationRequestedFromOnMapReadyWhenPReviousOneWasSaved() throws Exception {
        MainScreenModelImpl model = new MainScreenModelImpl( callback, testSharedPRefsWithSavedLocation, googleApiClient);
        model.onCurrentLocationRequested(true);
        Location expectedLocation = testSharedPRefsWithSavedLocation.getLastKnownLocation().toGoogleLocation();
        verify(callback, times(1)).onCurrentLocationReceived(expectedLocation);
    }

    @Test
    public void onLocationChanged() throws Exception {
        MainScreenModelImpl model = new MainScreenModelImpl(callback, sharedPrefs, googleApiClient);
        Location location = mock(Location.class);
        location.setLatitude(12);
        location.setLongitude(12);

        model.onLocationChanged(location);
        verify(callback, times(1)).onCurrentLocationReceived(location);
    }

}
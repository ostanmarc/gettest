package com.ostan.gettest.ui.mainscreen.detailspane;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;

import com.ostan.gettest.R;
import com.ostan.gettest.databinding.BottomSheetLayoutBinding;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.placedetails.PresentableDetails;

import org.parceler.Parcels;

import rx.Observer;

/**
 * Created by Obaro on 01/08/2016.
 */
public class PlaceDetailsSheet extends BottomSheetDialogFragment implements Observer<Bitmap> {

    private final static String ARG_DETAILS = "details";
    private final static String ARG_BITMAP = "bitmap";


    PresentableDetails presentableDetails;
    BottomSheetLayoutBinding binding;

    public static PlaceDetailsSheet newInstance(Details details, Bitmap bitmap) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_DETAILS, Parcels.wrap(details));
        bundle.putParcelable(ARG_BITMAP, bitmap);

        PlaceDetailsSheet fragment = new PlaceDetailsSheet();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();


        Details details = Parcels.unwrap(bundle.getParcelable(ARG_DETAILS));
        presentableDetails = new PresentableDetails(details);
        presentableDetails.setBitmap((Bitmap) bundle.getParcelable(ARG_BITMAP));

    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        binding  = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.bottom_sheet_layout, null, false);
        binding.setDetails(presentableDetails);
        dialog.setContentView(binding.getRoot());


    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(Bitmap bitmap) {
        binding.photo.setImageBitmap(bitmap);
    }
}

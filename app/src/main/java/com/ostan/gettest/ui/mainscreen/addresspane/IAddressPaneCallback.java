package com.ostan.gettest.ui.mainscreen.addresspane;

import com.ostan.gettest.models.addresses.AddressData;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public interface IAddressPaneCallback {
    /**
     * Notifies the address pane callback that route was requested
     * @param origin - route origin
     * @param desination - route destination
     */
    void onRouteRequested(AddressData origin, AddressData desination);
}

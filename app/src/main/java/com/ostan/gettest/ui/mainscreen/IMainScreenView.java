package com.ostan.gettest.ui.mainscreen;

import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

import java.util.List;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMainScreenView {
    void showPlaces(List<Place> markers);
    void centerOnPosition(LatLng location, float zoom);

    void onSearchResultArrived(Details details, float zoom);
    void drawRoute(Route route);

    void showLoading(boolean toShow);
    void showAddress(LocationType type, AddressData addressData);
}

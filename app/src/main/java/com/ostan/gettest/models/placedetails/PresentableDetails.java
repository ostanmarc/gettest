package com.ostan.gettest.models.placedetails;

import android.graphics.Bitmap;

/**
 * Created by marco on 17/05/2017.
 */

public class PresentableDetails extends Details {
    Bitmap bitmap;

    public PresentableDetails(Details details) {
        this.setAddressComponents(details.addressComponents);
        this.setFormattedAddress(details.formattedAddress);
        this.setGeometry(details.getGeometry());
        this.setName(details.getName());
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}

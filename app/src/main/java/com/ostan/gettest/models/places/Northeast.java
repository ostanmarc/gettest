
package com.ostan.gettest.models.places;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Northeast {

    @SerializedName("lat")
    @Expose
    protected Double lat;
    @SerializedName("lng")
    @Expose
    protected Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

}

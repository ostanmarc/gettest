package com.ostan.gettest.utils;

import com.ostan.gettest.models.places.LocationPojo;

/**
 * Created by marco on 17/05/2017.
 */

public interface IAppstateSharedPrefs {
    void setLastKnownLocation(LocationPojo location);
    LocationPojo getLastKnownLocation();
}

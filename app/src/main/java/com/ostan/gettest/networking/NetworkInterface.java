package com.ostan.gettest.networking;

import com.ostan.gettest.models.addresses.GeoCodingResponse;
import com.ostan.gettest.models.autocomplete.AutoCompleteResponse;
import com.ostan.gettest.models.placedetails.PlacesDetailsResponse;
import com.ostan.gettest.models.places.PlacesResponse;
import com.ostan.gettest.models.routes.RoutesResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by marco on 11/05/2017.
 */

public interface NetworkInterface {

    @GET("json")
    Observable<PlacesResponse> getNearbyPlaces(@Query("location")String loc, @Query("radius") int radius, @Query("key") String key, @Query("keyword") String querry);

    @GET("json")
    Observable<GeoCodingResponse> reverseLocation(@Query("latlng")String loc, @Query("key") String key);

    @GET("json")
    Call<AutoCompleteResponse> autoCompleteRequest(@Query("input") String input, @Query("location")String loc, @Query("key") String key);

    @GET("json")
    Observable<AutoCompleteResponse> autoCompleteRequestRx(@Query("input") String input, @Query("location")String loc, @Query("key") String key);


    @GET("json")
    Observable<RoutesResponse> routesRequestRx(@Query("origin")String srcLoc, @Query("destination")String dstLoc, @Query("key") String key);


    @GET("json")
    Observable<PlacesDetailsResponse> placeDeteailsRequest(@Query("placeid") String placeId, @Query("key") String key);

    @GET("photo")
    Call<ResponseBody> getImageDetails(@Query("photoreference") String reference, @Query("maxheight") String maxHeight, @Query("key") String key);

}

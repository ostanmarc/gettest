package com.ostan.gettest.ui.mainscreen.map;

import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.models.places.Place;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMapHostCallback {
    void onMapReady();
    void onMapCenterChanged(LatLng location);
    void onMapDragging(boolean isStarted);
    void onMarkerClicked(Place place);
}

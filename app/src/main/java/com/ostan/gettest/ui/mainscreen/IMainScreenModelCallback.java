package com.ostan.gettest.ui.mainscreen;

import android.location.Location;

import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

import java.util.List;

/**
 * Created by marco on 14/05/2017.
 */

public interface IMainScreenModelCallback {
    void onPlacesReceived(List<Place> places);
    void onRouteReceived(Route route);
    void onCurrentLocationReceived(Location location);
    void onAddressReceived(LocationType type, AddressData addressData);
    void onPlaceDetailsReceived(Details details);
}

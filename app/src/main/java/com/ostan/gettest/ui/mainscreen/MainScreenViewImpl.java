package com.ostan.gettest.ui.mainscreen;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.R;
import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.ui.mainscreen.addresspane.AddressPresenterImpl;
import com.ostan.gettest.ui.mainscreen.addresspane.IAddressPane;
import com.ostan.gettest.ui.mainscreen.addresspane.IAddressPaneHostCallback;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;
import com.ostan.gettest.ui.mainscreen.map.IMapHostCallback;
import com.ostan.gettest.ui.mainscreen.map.IMapController;
import com.ostan.gettest.ui.mainscreen.map.MapControllerImpl;

import java.util.List;

/**
 * Created by marco on 14/05/2017.
 */

public class MainScreenViewImpl implements IMainScreenView, IMapHostCallback, IAddressPaneHostCallback {


    IMapController mapController;
    IMainScreenPresenter presenter;
    ImageButton locateBtn, searchBtn;
    TextView addressTv;
    ImageView centerOfTheMapImv;


    IAddressPane addressPane;


    public MainScreenViewImpl(SupportMapFragment mapFragment, IMainScreenPresenter presenter) {
       initialize(mapFragment, presenter);
    }

    @Override
    public void showPlaces(List<Place> places) {
        mapController.addPlaces(places);
    }


    @Override
    public void centerOnPosition(LatLng latLng, float zoom) {
        mapController.centerOnPosition(latLng, zoom);
    }

    @Override
    public void onSearchResultArrived(Details details, float zoom) {
        mapController.onSearchAddressArrived(details, zoom);
    }

    @Override
    public void drawRoute(Route route) {
        mapController.drawRoute(route);
    }

    @Override
    public void showLoading(boolean toShow) {
        addressPane.loadingStarted(toShow);
    }

    @Override
    public void showAddress(LocationType type, AddressData addressData) {
        addressPane.loadingStarted(false);
        addressPane.onAddressReceived(type, addressData);
    }

    @Override
    public void onMapReady() {
        presenter.onMapReady();
    }

    @Override
    public void onMapCenterChanged(LatLng location) {
        presenter.onMapCenterChanged(location);
    }

    @Override
    public void onMapDragging(boolean isStarted) {
        centerOfTheMapImv.setVisibility(isStarted ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onMarkerClicked(Place place) {
        presenter.onMarkerClicked(place);
    }


    private void initialize(SupportMapFragment mapFragment, final IMainScreenPresenter presenter){
        locateBtn = (ImageButton) mapFragment.getActivity().findViewById(R.id.main_locate_btn);
        searchBtn = (ImageButton) mapFragment.getActivity().findViewById(R.id.main_search_btn);
        addressTv = (TextView) mapFragment.getActivity().findViewById(R.id.main_addr_tv);
        centerOfTheMapImv = (ImageView) mapFragment.getActivity().findViewById(R.id.main_map_center);

        addressPane = new AddressPresenterImpl((FrameLayout) mapFragment.getActivity().findViewById(R.id.main_address_holder), this);
        this.presenter = presenter;
        locateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLocationButtonClicked();
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSearchButtonClicked();
            }
        });
        mapController = new MapControllerImpl(this, mapFragment);
    }

    @Override
    public void onRoutesRequested(LocationPojo origin, LocationPojo destination) {
        presenter.onRouteButtonClicked(origin, destination);
    }
}

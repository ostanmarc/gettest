package com.ostan.gettest.ui.search;

/**
 * Created by marco on 17/12/2016.
 */

public interface ISearchPresenter<T> {
    /***
     * notify presenter that neew text was typed
     * @param currentQuerryString
     */
    void onQuerryTextEdited(String currentQuerryString);

    /**
     * notify presenter trhat one of the results was selecte
     * @param selectedResult - seclected result
     */
    void onResultClicked(T selectedResult);
}

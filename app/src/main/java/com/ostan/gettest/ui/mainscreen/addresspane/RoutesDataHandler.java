package com.ostan.gettest.ui.mainscreen.addresspane;

import android.util.Pair;

import com.ostan.gettest.models.addresses.AddressData;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public class RoutesDataHandler {

    public static enum LocationType{
        current,
        destination
    }

    AddressData[] addressDataPoints = new AddressData[2];

    public boolean shouldRouteOptionBeActive(){
         return addressDataPoints[0] != null && addressDataPoints[1] != null;
    }

    public void addressReceived(LocationType type, AddressData data){

        if(type == LocationType.current) {
            addressDataPoints[0] = data;
            addressDataPoints[1] = null;
            return;
        }

        addressDataPoints[1] = data;

    }

    public Pair<AddressData, AddressData> getRoutePoints(){
        return new Pair<>(addressDataPoints[0], addressDataPoints[1]);

    }
}

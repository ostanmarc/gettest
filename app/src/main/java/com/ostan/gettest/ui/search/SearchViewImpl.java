package com.ostan.gettest.ui.search;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.ostan.gettest.R;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.ui.search.recycler.RecyclerItemAdapter;

import java.util.List;

/**
 * Created by marco on 17/12/2016.
 */

public class SearchViewImpl implements ISearchView<Prediction>, IAdapterCallback<Prediction> {

    private ISearchPresenter presenter;

    RecyclerView recycler;

    RecyclerItemAdapter adapter;


    public SearchViewImpl(final ISearchPresenter presenter, View rootView) {
        this.presenter = presenter;

        recycler = (RecyclerView) rootView.findViewById(R.id.results_recycler_view);
        adapter = new RecyclerItemAdapter(rootView.getContext(), this);

        LinearLayoutManager llm = new LinearLayoutManager(rootView.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(llm);

        recycler.setAdapter(adapter);


    }

    @Override
    public void itemWasClicked(Prediction item) {
        presenter.onResultClicked(item);
    }

    @Override
    public void addAddressItem(List<Prediction> items) {
        adapter.clearItems();
        for (Prediction item: items) {
            adapter.addItem(item);
        }
    }

    @Override
    public void activateQuerryChangesListening(EditText querrySource) {
        querrySource.requestFocus();


        querrySource.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.onQuerryTextEdited(s.toString());
            }
        });
    }
}

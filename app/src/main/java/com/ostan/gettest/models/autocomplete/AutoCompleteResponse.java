
package com.ostan.gettest.models.autocomplete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AutoCompleteResponse {

    @SerializedName("predictions")
    @Expose
    protected List<Prediction> predictions = null;
    @SerializedName("status")
    @Expose
    protected String status;

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

package com.ostan.gettest.ui.mainscreen.addresspane;

import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public interface IAddressPanePresenter {

    /**
     * Notify presenter that route button has been clicked
     */
    void onRouteButtonClicked();
}

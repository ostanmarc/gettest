package com.ostan.gettest.ui.mainscreen;

import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.networking.APIConnector;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;
import com.ostan.gettest.ui.mainscreen.detailspane.PlaceDetailsSheet;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;

/**
 * Created by marco on 14/05/2017.
 */

public class MainScreenPresenterImpl implements IMainScreenPresenter, IMainScreenModelCallback {



    IMainScreenView view;
    IMainsScreenModel model;
    IMainScreenCallback callback;
    Location currentLocation;


    public MainScreenPresenterImpl(SupportMapFragment supportMapFragment, IMainScreenCallback callback) {
        this.view = new MainScreenViewImpl(supportMapFragment, this);
        this.model = new MainScreenModelImpl(this, callback.getActivity());
        view.showLoading(true);
        this.callback = callback;
    }

    public MainScreenPresenterImpl(IMainScreenCallback callback, IMainScreenView view, IMainsScreenModel model) {
        this.view = view;
        this.model = model;
        this.callback = callback;
    }

    @Override
    public void onMapReady() {
        model.onCurrentLocationRequested(true);
    }

    @Override
    public void onLocationButtonClicked() {
        model.onCurrentLocationRequested(false);
        view.showLoading(true);
    }

    @Override
    public void onSearchButtonClicked() {
       callback.onSearchRequested(currentLocation);
    }

    @Override
    public void onSearchResultReturned(Prediction prediction) {
        model.onPLaceDetailsRequested(prediction.getPlaceId());
    }

    @Override
    public void onStart() {
        model.onStart();
    }

    @Override
    public void onStop() {
        model.onStop();
    }

    @Override
    public void onMapCenterChanged(LatLng currentCenterLocation) {
        Location location = new Location("");
        location.setLatitude(currentCenterLocation.latitude);
        location.setLongitude(currentCenterLocation.longitude);
        model.onAddressRequested(LocationType.destination, location);
        model.onNearbyPlacesRequested(location);
        view.showLoading(true);
    }

    @Override
    public void onMarkerClicked(Place data) {
        model.onAddressRequested(LocationType.destination, data.getGeometry().getLocation().toGoogleLocation());
    }

    @Override
    public void onRouteButtonClicked(LocationPojo origin, LocationPojo destination) {
        view.showLoading(true);
        model.onRouteRequested(origin.toGoogleLocation(), destination.toGoogleLocation());
    }

    @Override
    public void onPlacesReceived(List<Place> places) {
        view.showPlaces(places);
        view.showLoading(false);
    }

    @Override
    public void onRouteReceived(Route route) {

        view.showLoading(false);
        view.drawRoute(route);
    }



    @Override
    public void onCurrentLocationReceived(Location location) {

        currentLocation = location;
        ArrayList<MarkerOptions> markers = new ArrayList<>();
        LatLng postiion = new LatLng(location.getLatitude(), location.getLongitude());

        view.centerOnPosition(postiion, 16f);

        model.onAddressRequested(LocationType.current, location);
        model.onNearbyPlacesRequested(location);

    }

    @Override
    public void onAddressReceived(LocationType type, AddressData addressData) {
        view.showAddress(type, addressData);
    }

    @Override
    public void onPlaceDetailsReceived(Details details) {
        view.onSearchResultArrived(details, 16f);
        view.showLoading(true);
        handlePlaceSearchResult(details);
    }



    private void handlePlaceSearchResult(final Details details){
        new APIConnector().getImage(details.getPhotos().get(0), new Observer<Bitmap>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Bitmap bitmap) {
                PlaceDetailsSheet bottomSheetDialogFragment = PlaceDetailsSheet.newInstance(details, bitmap);
                bottomSheetDialogFragment.show(callback.getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                view.showLoading(false);
            }
        });


    }


}

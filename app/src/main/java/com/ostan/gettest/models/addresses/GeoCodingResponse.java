
package com.ostan.gettest.models.addresses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeoCodingResponse {

    @SerializedName("results")
    @Expose
    protected List<AddressData> results = null;
    @SerializedName("status")
    @Expose
    protected String status;

    public List<AddressData> getResults() {
        return results;
    }

    public void setResults(List<AddressData> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}


package com.ostan.gettest.models.addresses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ostan.gettest.models.places.Northeast;
import com.ostan.gettest.models.places.Southwest;

import org.parceler.Parcel;

@Parcel
public class Bounds {

    @SerializedName("northeast")
    @Expose
    protected Northeast northeast;
    @SerializedName("southwest")
    @Expose
    protected Southwest southwest;

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }

}

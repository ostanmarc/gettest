package com.ostan.gettest.ui.mainscreen.map;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.utils.MapUtils;

import java.util.List;

/**
 * Created by marco on 14/05/2017.
 */

public class MapControllerImpl implements OnMapReadyCallback, IMapController {


    GoogleMap map;
    private LatLng cameraLastPosition;
    private PolylineOptions currentRoute;
    private List<Place> currentPlaces;

    private boolean isMapAutomatedMovingExpected = false;

    // Minimum distance value in km to enable route calculations
    private final double LOCATION_CHANGE_TRESHOLD = 0.1;

    IMapHostCallback mapHostCallback;


    public MapControllerImpl(IMapHostCallback mainScreenPresenter, SupportMapFragment map) {
        this.mapHostCallback = mainScreenPresenter;
        map.getMapAsync(this);
    }


    @Override
    public void addPlaces(List<Place> places) {
        map.clear();
        currentPlaces = places;
        drawCurrentPlaces();
        if (currentRoute != null) {
            map.addPolyline(currentRoute);
        }
    }

    @Override
    public void centerOnPosition(LatLng location, float zoom) {
        cameraLastPosition = location;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
        map.clear();
        drawCurrentPlaces();
        isMapAutomatedMovingExpected = false;
    }

    @Override
    public void onSearchAddressArrived(Details details, float zoom) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(details.getGeometry().getLocation().toLatLang(), zoom));
        currentPlaces.add(generatePlaceObjFromDetails(details));
        map.addMarker(generateMarker(details.getGeometry().getLocation().toLatLang(), details.getName()));
    }

    @Override
    public void drawRoute(Route route) {
        map.clear();
        drawCurrentPlaces();

        currentRoute = new PolylineOptions();
        currentRoute.addAll(MapUtils.decodePoly(route.getOverviewPolyline().getPoints()));
        map.addPolyline(currentRoute);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.getUiSettings().setMyLocationButtonEnabled(false);

        map.setMyLocationEnabled(true);

        mapHostCallback.onMapReady();

        cameraLastPosition = map.getCameraPosition().target;

        map.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mapHostCallback.onMarkerClicked((Place) marker.getTag());
                isMapAutomatedMovingExpected = true;
                return false;
            }
        });
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng currentMapPosition = map.getCameraPosition().target;

                if (MapUtils.calculationByDistance(currentMapPosition, cameraLastPosition) > LOCATION_CHANGE_TRESHOLD) {

                    if(!isMapAutomatedMovingExpected) {
                        mapHostCallback.onMapCenterChanged(currentMapPosition);
                    }
                    cameraLastPosition = currentMapPosition;
                }
                mapHostCallback.onMapDragging(false);
            }
        });
        map.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                if (i == REASON_GESTURE) {
                    mapHostCallback.onMapDragging(true);
                }
            }
        });
    }


    private void drawCurrentPlaces() {
        if (currentPlaces == null || currentPlaces.isEmpty()) {
            return;
        }
        for (Place placeData : currentPlaces) {
            MarkerOptions markerData = generateMarker(placeData.getGeometry().getLocation().toLatLang(), placeData.getName());
            map.addMarker(markerData).setTag(placeData);
        }
    }

    private MarkerOptions generateMarker(LatLng location, String name){
        return  new MarkerOptions().position(location)
        .title(name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
    }

    private Place generatePlaceObjFromDetails(Details details){
        Place place = new Place();
        place.setName(details.getName());
        place.setGeometry(details.getGeometry());

        return place;
    }
}

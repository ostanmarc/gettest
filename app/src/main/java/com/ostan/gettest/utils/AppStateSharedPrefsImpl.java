package com.ostan.gettest.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ostan.gettest.models.places.LocationPojo;

/**
 * Created by marc.ostan on 5/16/2017.
 */

public class AppStateSharedPrefsImpl implements IAppstateSharedPrefs  {

    private SharedPreferences sharedPRefs;
    private final String SP_FILE_NAME = "sharedPrefs";
    private final String KEY_LOCATION = "lastKnownLocation";



    public AppStateSharedPrefsImpl(Context context) {
        this.sharedPRefs = context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void setLastKnownLocation(LocationPojo location){
        Gson gson = new Gson();
        sharedPRefs.edit().putString(KEY_LOCATION, gson.toJson(location)).apply();
    }

    public LocationPojo getLastKnownLocation(){
        Gson gson = new Gson();
        String serialized = sharedPRefs.getString(KEY_LOCATION, null);
        return serialized != null ? gson.fromJson(serialized, LocationPojo.class) : null;
    }
}

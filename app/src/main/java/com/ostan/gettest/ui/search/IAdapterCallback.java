package com.ostan.gettest.ui.search;

/**
 * Created by marco on 18/12/2016.
 */

public interface IAdapterCallback<T> {
    void itemWasClicked(T item);
}

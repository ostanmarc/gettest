package com.ostan.gettest.ui.search.recycler;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ostan.gettest.R;
import com.ostan.gettest.databinding.SearchResultItemBinding;

;


/**
 * Created by marco on 15/07/2016.
 */
public class ItemUIFactory {

    public static ItemViewHolder createUIForSession(ViewGroup parent, int itemType, IResultsEventsCallback listener) {

        ItemViewHolder vh = null;
        switch (itemType) {
            case 0: {
                SearchResultItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.search_result_item, parent, false);
                vh = new ItemViewHolder(listener, binding);
            }
            break;
            case 1: {
                // NOT IN USE IN CURRENT IMPLEMENTATION
            }
            break;
            case 2: {
                // NOT IN USE IN CURRENT IMPLEMENTATION
            }

        }

        return vh;
    }
}

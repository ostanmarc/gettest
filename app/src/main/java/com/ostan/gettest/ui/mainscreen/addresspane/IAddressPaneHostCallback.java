package com.ostan.gettest.ui.mainscreen.addresspane;

import com.ostan.gettest.models.places.LocationPojo;

/**
 * Created by marco on 16/05/2017.
 */

public interface IAddressPaneHostCallback {
    /**
     * Notifies the address pane host that route was requested
     * @param origin - route origin
     * @param destination - route destination
     */
    void onRoutesRequested(LocationPojo origin, LocationPojo destination);
}

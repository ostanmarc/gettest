package com.ostan.gettest.models.places;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by marco on 13/05/2017.
 */

public class PlacesResponse {

    @SerializedName("html_attributions")
    @Expose
    protected List<Object> htmlAttributions = null;
    @SerializedName("next_page_token")
    @Expose
    protected String nextPageToken;
    @SerializedName("results")
    @Expose
    protected List<Place> results = null;
    @SerializedName("status")
    @Expose
    protected String status;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public List<Place> getResults() {
        return results;
    }

    public void setResults(List<Place> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}

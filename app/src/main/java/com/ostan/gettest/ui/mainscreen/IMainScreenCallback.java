package com.ostan.gettest.ui.mainscreen;

import android.location.Location;
import android.support.v4.app.FragmentActivity;

/**
 * Created by marco on 17/05/2017.
 */

public interface IMainScreenCallback {
    void onSearchRequested(Location location);
    FragmentActivity getActivity();
}

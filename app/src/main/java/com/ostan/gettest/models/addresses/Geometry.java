
package com.ostan.gettest.models.addresses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.models.places.Viewport;

import org.parceler.Parcel;

@Parcel
public class Geometry {

    @SerializedName("location")
    @Expose
    protected LocationPojo location;
    @SerializedName("location_type")
    @Expose
    protected String locationType;
    @SerializedName("viewport")
    @Expose
    protected Viewport viewport;
    @SerializedName("bounds")
    @Expose
    protected Bounds bounds;

    public LocationPojo getLocation() {
        return location;
    }

    public void setLocation(LocationPojo location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

    public Bounds getBounds() {
        return bounds;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

}

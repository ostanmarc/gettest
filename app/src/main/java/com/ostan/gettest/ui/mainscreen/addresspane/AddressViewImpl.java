package com.ostan.gettest.ui.mainscreen.addresspane;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ostan.gettest.R;
import com.ostan.gettest.models.addresses.AddressData;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public class AddressViewImpl implements IAddressPaneView {


    IAddressPanePresenter presenter;

    final FrameLayout rootLayout;
    ProgressBar progressBar;
    Button routeButton;
    TextView addressTv;

    public AddressViewImpl(FrameLayout rootLayout, final IAddressPanePresenter presenter) {
        this.rootLayout = rootLayout;
        progressBar = (ProgressBar) rootLayout.findViewById(R.id.address_pane_progress);
        routeButton = (Button) rootLayout.findViewById(R.id.main_route_btn);
        addressTv = (TextView) rootLayout.findViewById(R.id.main_addr_tv);
        this.presenter = presenter;

        routeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onRouteButtonClicked();
            }
        });
    }

    @Override
    public void showAddress(AddressData addressData) {
        if(addressData == null) {
            hide(true);
        } else {
            hide(false);
            addressTv.setText(addressData.getFormattedAddress());
        }
    }

    @Override
    public void showRouteButton(boolean toShow) {
        routeButton.setVisibility(toShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showLoading(boolean toShow) {
        progressBar.setVisibility(toShow ? View.VISIBLE : View.GONE);
    }

    private void hide(boolean toHide){
        addressTv.setVisibility(toHide ? View.GONE : View.VISIBLE);
    }
}

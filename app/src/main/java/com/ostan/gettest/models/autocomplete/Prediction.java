
package com.ostan.gettest.models.autocomplete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Prediction {

    @SerializedName("description")
    @Expose
    protected String description;
    @SerializedName("id")
    @Expose
    protected String id;
    @SerializedName("matched_substrings")
    @Expose
    protected List<MatchedSubstring> matchedSubstrings = null;
    @SerializedName("place_id")
    @Expose
    protected String placeId;
    @SerializedName("reference")
    @Expose
    protected String reference;
    @SerializedName("structured_formatting")
    @Expose
    protected StructuredFormatting structuredFormatting;
    @SerializedName("terms")
    @Expose
    protected List<Term> terms = null;
    @SerializedName("types")
    @Expose
    protected List<String> types = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MatchedSubstring> getMatchedSubstrings() {
        return matchedSubstrings;
    }

    public void setMatchedSubstrings(List<MatchedSubstring> matchedSubstrings) {
        this.matchedSubstrings = matchedSubstrings;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public StructuredFormatting getStructuredFormatting() {
        return structuredFormatting;
    }

    public void setStructuredFormatting(StructuredFormatting structuredFormatting) {
        this.structuredFormatting = structuredFormatting;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}

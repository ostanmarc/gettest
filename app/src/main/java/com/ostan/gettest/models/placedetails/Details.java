
package com.ostan.gettest.models.placedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ostan.gettest.models.addresses.AddressComponent;
import com.ostan.gettest.models.addresses.Geometry;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Details {

    @SerializedName("address_components")
    @Expose
    protected List<AddressComponent> addressComponents = null;
    @SerializedName("adr_address")
    @Expose
    protected String adrAddress;
    @SerializedName("formatted_address")
    @Expose
    protected String formattedAddress;
    @SerializedName("formatted_phone_number")
    @Expose
    protected String formattedPhoneNumber;
    @SerializedName("geometry")
    @Expose
    protected Geometry geometry;
    @SerializedName("icon")
    @Expose
    protected String icon;
    @SerializedName("id")
    @Expose
    protected String id;
    @SerializedName("international_phone_number")
    @Expose
    protected String internationalPhoneNumber;
    @SerializedName("name")
    @Expose
    protected String name;
    @SerializedName("photos")
    @Expose
    protected List<Photo> photos = null;
    @SerializedName("place_id")
    @Expose
    protected String placeId;
    @SerializedName("rating")
    @Expose
    protected Double rating;
    @SerializedName("reference")
    @Expose
    protected String reference;
    @SerializedName("reviews")
    @Expose
    protected List<Review> reviews = null;
    @SerializedName("scope")
    @Expose
    protected String scope;
    @SerializedName("types")
    @Expose
    protected List<String> types = null;
    @SerializedName("url")
    @Expose
    protected String url;
    @SerializedName("utc_offset")
    @Expose
    protected Integer utcOffset;
    @SerializedName("vicinity")
    @Expose
    protected String vicinity;
    @SerializedName("website")
    @Expose
    protected String website;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getAdrAddress() {
        return adrAddress;
    }

    public void setAdrAddress(String adrAddress) {
        this.adrAddress = adrAddress;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public void setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInternationalPhoneNumber() {
        return internationalPhoneNumber;
    }

    public void setInternationalPhoneNumber(String internationalPhoneNumber) {
        this.internationalPhoneNumber = internationalPhoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Integer utcOffset) {
        this.utcOffset = utcOffset;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

}

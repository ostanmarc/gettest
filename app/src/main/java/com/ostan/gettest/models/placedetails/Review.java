
package com.ostan.gettest.models.placedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Review {

    @SerializedName("author_name")
    @Expose
    protected String authorName;
    @SerializedName("author_url")
    @Expose
    protected String authorUrl;
    @SerializedName("language")
    @Expose
    protected String language;
    @SerializedName("profile_photo_url")
    @Expose
    protected String profilePhotoUrl;
    @SerializedName("rating")
    @Expose
    protected Integer rating;
    @SerializedName("relative_time_description")
    @Expose
    protected String relativeTimeDescription;
    @SerializedName("text")
    @Expose
    protected String text;
    @SerializedName("time")
    @Expose
    protected Integer time;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getRelativeTimeDescription() {
        return relativeTimeDescription;
    }

    public void setRelativeTimeDescription(String relativeTimeDescription) {
        this.relativeTimeDescription = relativeTimeDescription;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

}

package com.ostan.gettest.ui.search.recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.ostan.gettest.ui.search.IAdapterCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marco on 11/07/2016.
 */
public abstract class AbstractSearchResultsViewAdapter<VH extends ItemViewHolder, T>
        extends RecyclerView.Adapter<VH> implements IResultsEventsCallback<T> {

    protected Context context;

    List<T> dataItems;
    IAdapterCallback callback;

    public AbstractSearchResultsViewAdapter(final Context context, IAdapterCallback callback) {
        this.context = context;
        this.dataItems = new ArrayList<>();
        this.callback = callback;
    }


    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public abstract void onBindViewHolderConcrete(VH holder, T item);

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        onBindViewHolderConcrete(viewHolder, dataItems.get(position));
    }

    public void clearItems(){
        dataItems.clear();
        notifyDataSetChanged();
    }
    public void addItem(T item) {
        dataItems.add(item);
        notifyItemInserted(dataItems.size());
    }

}

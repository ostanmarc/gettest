package com.ostan.gettest.ui.search;

import android.widget.EditText;

import java.util.List;

/**
 * Created by marco on 17/12/2016.
 */

public interface ISearchView<T> {
    /***
     * Provide the view with the results of the search
     * @param items
     */
    void addAddressItem(List<T> items);

    /***
     * Activate listening tio the text changes on the provided field
     * @param querrySource
     */
    void activateQuerryChangesListening(EditText querrySource);
}

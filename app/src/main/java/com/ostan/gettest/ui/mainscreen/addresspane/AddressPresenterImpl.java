package com.ostan.gettest.ui.mainscreen.addresspane;

import android.util.Pair;
import android.widget.FrameLayout;

import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;

/**
 * Created by marc.ostan on 5/15/2017.
 */

public class AddressPresenterImpl implements IAddressPanePresenter, IAddressPane {

    RoutesDataHandler routesDataHandler;
    IAddressPaneView view;
    IAddressPaneHostCallback callback;

    public AddressPresenterImpl(FrameLayout frameLayout, IAddressPaneHostCallback callback) {
        this.view = new AddressViewImpl(frameLayout, this);
        this.callback = callback;
        routesDataHandler = new RoutesDataHandler();

    }

    @Override
    public void onAddressReceived(LocationType type, AddressData data) {
        routesDataHandler.addressReceived(type, data);
        view.showAddress(data);
        view.showRouteButton(routesDataHandler.shouldRouteOptionBeActive());
    }

    @Override
    public void loadingStarted(boolean isStarted) {
        view.showLoading(isStarted);
    }

    @Override
    public void onRouteButtonClicked() {
        Pair<AddressData, AddressData> pair  = routesDataHandler.getRoutePoints();
        callback.onRoutesRequested(pair.first.getGeometry().getLocation(), pair.second.getGeometry().getLocation());
    }
}

package com.ostan.gettest.ui.mainscreen;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 17/05/2017.
 */
public class MainScreenPresenterImplTest {


    IMainScreenView view = mock(MainScreenViewImpl.class);
    IMainsScreenModel model = mock(MainScreenModelImpl.class);
    IMainScreenCallback callback  = mock(MapsActivity.class);

    MainScreenPresenterImpl presenter = new MainScreenPresenterImpl(callback, view, model);
    Location location = mock(Location.class);
    @Test
    public void testTriggeringPlacesAndAddressReversoCodingWhenLocationReceived(){

        location.setLatitude(12d);
        location.setLongitude(12d);
        presenter.onCurrentLocationReceived(location);

        verify(model, times(1)).onAddressRequested(RoutesDataHandler.LocationType.current, location);
        verify(model, times(1)).onNearbyPlacesRequested(location);
        verify(view, times(1)).centerOnPosition(new LatLng(location.getLatitude(), location.getLongitude()), 16f);

    }


    @Test
    public void testTriggeringPlaceDEtailsREquestAfterSearch(){
        String predictedPLaceId = "www";
        Prediction prediction = new Prediction();
        prediction.setPlaceId(predictedPLaceId);
        presenter.onSearchResultReturned(prediction);
        verify(model, times(1)).onPLaceDetailsRequested(prediction.getPlaceId());
    }

    @Test
    public void testLocationButtonClick(){
        presenter.onLocationButtonClicked();

        verify(model, times(1)).onCurrentLocationRequested(false);
        verify(view, times(1)).showLoading(true);
    }

    @Test
    public void testSearchButtonClicked(){
        presenter.onCurrentLocationReceived(location);
        presenter.onSearchButtonClicked();
        verify(callback, times(1)).onSearchRequested(location);
    }

}
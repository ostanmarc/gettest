package com.ostan.gettest.ui.mainscreen;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.ostan.gettest.models.addresses.AddressData;
import com.ostan.gettest.models.placedetails.Details;
import com.ostan.gettest.models.places.LocationPojo;
import com.ostan.gettest.models.places.Place;
import com.ostan.gettest.models.places.PlacesResponse;
import com.ostan.gettest.models.routes.Route;
import com.ostan.gettest.networking.APIConnector;
import com.ostan.gettest.ui.mainscreen.addresspane.RoutesDataHandler.LocationType;
import com.ostan.gettest.utils.AppStateSharedPrefsImpl;
import com.ostan.gettest.utils.IAppstateSharedPrefs;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by marco on 14/05/2017.
 */

public class MainScreenModelImpl implements IMainsScreenModel, ConnectionCallbacks, OnConnectionFailedListener,
        LocationListener {

    IMainScreenModelCallback callback;
    FragmentActivity fragmentActivity;
    APIConnector connector;
    IAppstateSharedPrefs stateSharedPrefs;
    GoogleApiClient mGoogleApiClient;

    //


    public MainScreenModelImpl(IMainScreenModelCallback callback, FragmentActivity fragmentActivity) {
        this.callback = callback;
        this.fragmentActivity = fragmentActivity;


        stateSharedPrefs = new AppStateSharedPrefsImpl(fragmentActivity);
        connector = new APIConnector();
        mGoogleApiClient = new GoogleApiClient.Builder(fragmentActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    // Used for testing purposes only
    public MainScreenModelImpl(IMainScreenModelCallback callback,  IAppstateSharedPrefs stateSharedPrefs, GoogleApiClient googleApiClient) {
        this.callback = callback;
        this.stateSharedPrefs = stateSharedPrefs;
        this.mGoogleApiClient = googleApiClient;
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        stopLocationUpdates();
    }

    @Override
    public void onCurrentLocationRequested(boolean isCalledFromOnMapReady) {
        if(isCalledFromOnMapReady) {
            LocationPojo location = stateSharedPrefs.getLastKnownLocation();
            if(location != null) {
                callback.onCurrentLocationReceived(location.toGoogleLocation());
                return;
            }
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if(location != null) {
            callback.onCurrentLocationReceived(location);
            return;
        }


    }

    @Override
    public void onNearbyPlacesRequested(Location location) {
        connector.getNearbyPlaces(location, "", new Observer<PlacesResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(PlacesResponse placesResponse) {
                Observable.from(placesResponse.getResults())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Place>() {

                            List<Place> markerOptionsList = new ArrayList<Place>();

                            @Override
                            public void onCompleted() {
                                callback.onPlacesReceived(markerOptionsList);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Place place) {
                                markerOptionsList.add(place);
                            }
                        });
            }
        });
    }

    @Override
    public void onAddressRequested(final LocationType type, Location location) {
        connector.getAddressByLocation(location, new Observer<AddressData>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(AddressData addressData) {
                callback.onAddressReceived(type, addressData);
            }
        });
    }

    @Override
    public void onRouteRequested(Location origin, Location destination) {
        connector.getRoutesRx(origin, destination, new Observer<List<Route>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Route> routes) {
                callback.onRouteReceived(routes.get(0));
            }
        });
    }

    @Override
    public void onPLaceDetailsRequested(String placeId) {

        connector.getPLaceDEtails(placeId, new Observer<Details>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Details details) {
                callback.onPlaceDetailsReceived(details);
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, LocationRequest.create(), this);
    }

    protected void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        callback.onCurrentLocationReceived(location);
        stateSharedPrefs.setLastKnownLocation(LocationPojo.fromGoogleLocation(location));
        stopLocationUpdates();
    }
}

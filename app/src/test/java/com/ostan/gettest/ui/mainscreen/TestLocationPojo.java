package com.ostan.gettest.ui.mainscreen;

import android.location.Location;

import com.ostan.gettest.models.places.LocationPojo;

import static org.mockito.Mockito.mock;

/**
 * Created by marco on 17/05/2017.
 */

class TestLocationPojo extends LocationPojo {

    Location location;

    public TestLocationPojo(double lat, double lon) {
        setLat(lat);
        setLng(lon);
        location = mock(Location.class);
        location.setLatitude(lat);
        location.setLongitude(lon);
    }

    @Override
    public Location toGoogleLocation() {
        return location;
    }
}

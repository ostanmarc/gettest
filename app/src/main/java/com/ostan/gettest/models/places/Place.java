
package com.ostan.gettest.models.places;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ostan.gettest.models.addresses.Geometry;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Place {

    @SerializedName("geometry")
    @Expose
    protected Geometry geometry;
    @SerializedName("icon")
    @Expose
    protected String icon;
    @SerializedName("id")
    @Expose
    protected String id;
    @SerializedName("name")
    @Expose
    protected String name;
    @SerializedName("photos")
    @Expose
    protected List<Photo> photos = null;
    @SerializedName("place_id")
    @Expose
    protected String placeId;
    @SerializedName("reference")
    @Expose
    protected String reference;
    @SerializedName("scope")
    @Expose
    protected String scope;
    @SerializedName("types")
    @Expose
    protected List<String> types = null;
    @SerializedName("vicinity")
    @Expose
    protected String vicinity;
    @SerializedName("opening_hours")
    @Expose
    protected OpeningHours openingHours;
    @SerializedName("rating")
    @Expose
    protected Double rating;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

}

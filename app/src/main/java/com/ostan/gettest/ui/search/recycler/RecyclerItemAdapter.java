package com.ostan.gettest.ui.search.recycler;


import android.content.Context;
import android.view.ViewGroup;

import com.ostan.gettest.models.autocomplete.Prediction;
import com.ostan.gettest.ui.search.IAdapterCallback;


public class RecyclerItemAdapter extends AbstractSearchResultsViewAdapter<ItemViewHolder, Prediction>  {


    public RecyclerItemAdapter(final Context context, IAdapterCallback callback) {
        super(context, callback);
    }

    @Override
    public void onBindViewHolderConcrete(ItemViewHolder holder, Prediction item) {
        holder.bindViewItemImpl(item);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }



    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Prediction item = dataItems.get(position);
        holder.bindViewItemImpl(item);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ItemUIFactory.createUIForSession(parent, viewType, this);
    }

    @Override
    public void onResultWasClicked(Prediction item) {
        callback.itemWasClicked(item);
    }
}

package com.ostan.gettest.ui.search;

import android.widget.EditText;

import java.util.List;

/**
 * Created by marco on 17/12/2016.
 */

public interface ISearchController<T> {
    /***
     * Notify search callback that new results arrived
     * @param items - results
     */
    void onNewResponseArrived(List<T> items);

    /***
     * provide search controller with search src field
     * @param editText
     */
    void setSearchField(EditText editText);
}


package com.ostan.gettest.models.autocomplete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class MainTextMatchedSubstring {

    @SerializedName("length")
    @Expose
    protected Integer length;
    @SerializedName("offset")
    @Expose
    protected Integer offset;

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

}
